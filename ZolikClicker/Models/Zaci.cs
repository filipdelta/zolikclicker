﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZolikClicker.Models
{
    public class Zaci
    {
        public Zaci()
        {
            ApplicationUser = new HashSet<ApplicationUser>();
        }
        public int Id { get; set; }
        public int PocetZaku { get; set; }

        public string ApplicationUserID { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
    }
}