﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZolikClicker.Models
{
    public class Zoliky
    {
        public Zoliky()
        {
            ApplicationUser = new HashSet<ApplicationUser>();
        }
        public int Id { get; set; }
        public int PocetZoliku { get; set; }

        public string ApplicationUserID { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
    }
}