﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZolikClicker.Models
{
    public class Testy
    {
        public Testy()
        {
            ApplicationUser = new HashSet<ApplicationUser>();
        }
        public int Id { get; set; }
        public int PocetTestu { get; set; }

        public string ApplicationUserID { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
    }
}