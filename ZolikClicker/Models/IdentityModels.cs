﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ZolikClicker.Models
{
    // Přidáním dalších vlastností do třídy uživatelů můžete přidat data profilu uživatele. Další informace najdete na webu https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Zoliky = new HashSet<Zoliky>();
            Testy = new HashSet<Testy>();
            Zaci = new HashSet<Zaci>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Poznámka: Objekt authenticationType musí odpovídat objektu definovanému objektem CookieAuthenticationOptions.AuthenticationType.
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Zde přidat vlastní deklarace uživatele
            return userIdentity;
        }
        public virtual ICollection<Zoliky> Zoliky { get; set; }
        public virtual ICollection<Testy> Testy { get; set; }
        public virtual ICollection<Zaci> Zaci { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public virtual DbSet<Zoliky> Zoliky { get; set; }
        public virtual DbSet<Testy> Testy { get; set; }
        public virtual DbSet<Zaci> Zaci { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}