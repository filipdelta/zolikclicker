﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZolikClicker.Models;

namespace ZolikClicker.Extensions
{
    public static class DataExtension
    {
        #region Zoliky
        public static string GetZoliky()
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();
            IEnumerable<Zoliky> projects = new List<Zoliky>();

            var zoliky = context.Zoliky.FirstOrDefault(x => x.ApplicationUserID == userid);

            if (zoliky == null)
            {
                zoliky = new Zoliky();
                zoliky.PocetZoliku = 0;
                zoliky.ApplicationUserID = userid;
                context.Zoliky.Add(zoliky);
                context.SaveChanges();
            }

            return zoliky.PocetZoliku.ToString();
        }

        public static void ChangeZoliky(int value)
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();

            // Zkontroluj existenci -> smaz
            var existingObj = context.Zoliky.FirstOrDefault(x => x.ApplicationUserID == userid);
            if (existingObj != null)
                context.Zoliky.Remove(existingObj);
            context.SaveChanges();

            // Vytvor novy
            existingObj.PocetZoliku = value;
            context.Zoliky.Add(existingObj);
            context.SaveChanges();
        }
        #endregion

        #region Testy
        public static string GetTesty()
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();

            var testy = context.Testy.FirstOrDefault(x => x.ApplicationUserID == userid);

            if (testy == null)
            {
                testy = new Testy();
                testy.PocetTestu = 1;
                testy.ApplicationUserID = userid;
                context.Testy.Add(testy);
                context.SaveChanges();
            }

            return testy.PocetTestu.ToString();
        }

        public static void ChangeTesty(int value)
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();

            // Zkontroluj existenci -> smaz
            var existingObj = context.Testy.FirstOrDefault(x => x.ApplicationUserID == userid);
            if (existingObj != null)
                context.Testy.Remove(existingObj);
            context.SaveChanges();

            // Vytvor novy
            existingObj.PocetTestu = value;
            context.Testy.Add(existingObj);
            context.SaveChanges();
        }
        #endregion

        #region Zaci
        public static string GetZaci()
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();

            var zaci = context.Zaci.FirstOrDefault(x => x.ApplicationUserID == userid);

            if (zaci == null)
            {
                zaci = new Zaci();
                zaci.PocetZaku = 0;
                zaci.ApplicationUserID = userid;
                context.Zaci.Add(zaci);
                context.SaveChanges();
            }

            return zaci.PocetZaku.ToString();
        }

        public static void ChangeZaci(int value)
        {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();

            // Zkontroluj existenci -> smaz
            var existingObj = context.Zaci.FirstOrDefault(x => x.ApplicationUserID == userid);
            if (existingObj != null)
                context.Zaci.Remove(existingObj);
            context.SaveChanges();

            // Vytvor novy
            existingObj.PocetZaku = value;
            context.Zaci.Add(existingObj);
            context.SaveChanges();
        }
        #endregion
    }
}