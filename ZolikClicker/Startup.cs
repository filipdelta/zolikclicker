﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZolikClicker.Startup))]
namespace ZolikClicker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
