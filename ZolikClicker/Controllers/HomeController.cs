﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZolikClicker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/Game/");
            //return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hra pro celou rodinu :)";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Nekontaktujte mě, prosím :x";

            return View();
        }
    }
}